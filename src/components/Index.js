import React from 'react';
import Form from './Form';
import List from './List';
import ChangeListState from './ChangeListState';
import Header from '../layouts/Header.js';

const Index = () => {
  const { notes, addItem, markAsComplete, deleteItem } = ChangeListState([]);

  return (
    <div className="App">
      <Header />

      <Form
        save={note => {
          addItem(note);
        }}
      />

      <List notes={notes} markAsComplete={markAsComplete} deleteItem={deleteItem} />
    </div>
  );
};

export default Index;