import { useState } from 'react';

const ChangeListState = (initialValue) => {

  const [notes, save] = useState(initialValue);

  return {
    notes,
    addItem: (note) => {
      save([...notes, note]);
    },

    deleteItem: noteIndex => {
      const newList = notes.filter((_, index) => index !== noteIndex);
      save(newList);
    },

    markAsComplete: noteIndex => {
      //get all items others thn which is marked for completion
      var allItems = notes.filter((_, index) => index !== noteIndex);

      //change value of item marked for completion
      const newList = notes.find((_, index) => index === noteIndex);
      newList.complete = true;

      //store in all items list
      allItems.push(newList);

      save(allItems);
    }
  };
};

export default ChangeListState;