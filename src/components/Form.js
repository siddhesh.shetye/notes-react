import React from 'react';
import ChangeInputState from './ChangeInputState';
import Swal from 'sweetalert2';

const Form = ({ save }) => {
  const { value, reset, onChange } = ChangeInputState();

  const handleSubmit = e => {
    e.preventDefault();

    if (validate()) {
      save({ value, complete: false });
      reset();
    }
  }

  const validate = () => {
    if (value.trim().length <= 0 || value.trim().length > 30) {
      Swal.fire({
        title: 'Oops!',
        text: 'Total characters length between 1 - 30 allowed !',
        icon: 'error',
        confirmButtonText: 'Yeah'
      })

      return false;
    }

    return true;
  }

  return (
    <div className="container-fluid page-body-wrapper">
      <div className="main-panel">
        <div className="content-wrapper">
          <div className="row justify-content-center">
            <div className="col-md-6 grid-margin stretch-card">
              <form onSubmit={handleSubmit} >

                <div className="form-group row">
                  <div className="col-md-10 col-sm-12">
                    <input type="text" className="form-control" placeholder="enter title" onChange={onChange} value={value} ></input>
                  </div>
                  <div className="col-md-2 col-sm-12 pt-btn-15">
                    <button className="btn header-bg text-white"> Submit </button>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};

export default Form;
