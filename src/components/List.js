import React from 'react';

const List = ({ notes, markAsComplete, deleteItem }) => {
  return (
    <div className="container" >
      <div className="row">
        <div className="col-lg-12">
          <div className="card bg-shadow">
            <div className="card-body">
              <div className="table-responsive">
                <table className="table table-hover">
                  <tbody>
                    {notes.map((record, index) => (
                      <tr key={index.toString()}>
                        <td> {record.value} </td>
                        <td className="text-center"> <label className={"badge " + (record.complete ? "badge-success" : "badge-warning")}> {record.complete ? "Complete" : "In Progress"} </label></td>

                        <td className="text-center">
                          {record.complete ? "" :
                            <button className="btn btn-success btn-sm" aria-label="Complete"
                              onClick={() => {
                                markAsComplete(index);
                              }}> Complete
                            </button>
                          }

                          <button className="btn btn-danger btn-sm" aria-label="Delete"
                            onClick={() => {
                              deleteItem(index);
                            }}> Delete
                          </button>
                        </td>
                      </tr>
                    ))}
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div >
  );
}
export default List;
