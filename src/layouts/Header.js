import React from 'react';

const Header = () => {
  return (
    <div className="header">
      <div className="header-bg bg-shadow">
        <div className="text-center pt-15 pb-15">
          <p className="mb-0 text-white font-weight-medium mb-2 mb-lg-0 mb-xl-0">Notes using Hooks Reactjs</p>
        </div>
      </div>
    </div>
  );
}

export default Header;